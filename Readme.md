# DOMP

## WFFLIX

- Media about the different courses at ADSD Windesheim Flevoland

The different modules will contain several video's that will  support the various assignments.

### Web Development
All about the web developmen, HTML, CSS and JavaScript. **WFFLIX** will also cover NPM, NodeJS, Express, PUG
### Mobile Development
In the beginning we will only cover React. As we will be talking about React, we will also take a quick look at PWA(Progressive Web Apps)
### OO Programming
Laravel is a web application framework with expressive, elegant syntax. We’ve already laid the foundation — freeing you to create without sweating the small things.
### Databases
MySQL is an Oracle-backed open source relational database management system (RDBMS) based on Structured Query Language (SQL). MySQL runs on virtually all platforms, including Linux, UNIX and Windows. Although it can be used in a wide range of applications, MySQL is most often associated with web applications and online publishing.

### C#
C# (C-Sharp) is a programming language developed by Microsoft that runs on the .NET Framework.
C# is used to develop web apps, desktop apps, mobile apps, games and much more.

### Agile
Agile is the ability to create and respond to change. It is a way of dealing with, and ultimately succeeding in, an uncertain and turbulent environment.

The authors of the Agile Manifesto chose “Agile” as the label for this whole idea because that word represented the adaptiveness and response to change which was so important to their approach.

It’s really about thinking through how you can understand what’s going on in the environment that you’re in today, identify what uncertainty you’re facing, and figure out how you can adapt to that as you go along.


## IDE
Jetbrains
1. phpStorm - editor for PHP(php, javascript, html, css, git, server-side development)
2. webStorm - editor for web(javascript, html, css)
3. YouTrack - SCRUM management
4. Microsoft Visual Studio Code - editor for web(javascript, html, css)