<?php


require 'views/elements/head.php';
require 'views/elements/nav.php';
?>
    <div class="container-fluid p-5">

        <div class="row">
            <div class="col-2"></div>
            <div class="col-8">
                <hr class="-ruler-horizontal">
                <form method="post" action="cs-upd">
                    <input type="hidden" name="id" value="<?= $result['id']; ?>">
                    <div class="form-outline mb-4">
                        <input type="text" id="title" name="title" class="form-control" value="<?= $result['title']; ?>" />
                        <label class="form-label" for="title">Title</label>
                    </div>

                    <div class="form-outline mb-4">
                        <input type="text" id="title" name="title_link" class="form-control" value="<?= $result['title_link']; ?>" />
                        <label class="form-label" for="title">Link to page</label>
                    </div>

<!--                     Description input -->
                    <div class="form-outline mb-4">
                        <textarea class="form-control" name="description" id="description" rows="4"><?= $result['description']; ?></textarea>
                        <label class="form-label" for="description">Explanation</label>
                    </div>

                    <!-- Submit button -->
                    <button type="submit" class="btn btn-primary btn-block mb-4">Save</button>
                </form>
            </div>
            <div class="col-2"></div>
        </div>
    </div>

<?php  require 'views/elements/tail.php';