<?php
require 'views/elements/head.php';
require "views/elements/nav.php";
?>
    <div class="container-fluid p-5">
        <div class="row">
            <?php require 'views/elements/cardLocalhost.php'; ?>
        </div><!-- end row -->
        <div class="row">
            <div style="text-align:center">
                <h1 class="h1">Array</h1>

                <div style="text-align:center">
                    <div class="ratio ratio-16x9">
                        <iframe width="560" height="315" src="https://www.youtube.com/embed/qbBrS7V85uY" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">

            <div class="card">
                <div class="card-title"><h1 class="h1">Voorbeeld Array</h1></div>
                <img class="card-img" style="width: 65%" src="views/img/php-array2.png" alt="php array">
                <div class="card-img-overlay">
                    <div class="card-body">



                        <?php

                        echo "<br>";


                        $kites = ['Gaastra', 'F-One', 'Cabrinha', 'Gin'];
                        $f1teams = array('Red Bull', 'Mclaren', 'Ferrari');

                        var_dump($kites);
                        echo "<br>";
                        var_dump($f1teams);
                        echo "<br>";
                        echo '<br>Bij Array wordt er een <strong>zero based</strong> index gebruikt. Dit betekend dat de waardes binnen een array beginnen me een 0!
                          <br>Om de eerste waarde van de kites array te laten zien doen we echo $kites[0]; of de laatste waarde van de fiteams echo $f1teams[?welke waarde?]; ';
                        echo "<br>Er word vaak een loop gebruikt voor het laten zien van de waardes binnen een array. Voor de kites gebruiken we een foreach loop";
                        echo "
                        <br>Voor de f1teams gebruiken we een for loop
                        <h2 class='h2'>Loops</h2>
                        <h3 class='h3'>Foreach</h3>";

                        foreach ($kites as $kite) {
                            echo "$kite <br>";
                        }

                        echo "<h3 class='h3'>For</h3>";
                        for ($x = 0; $x < count($f1teams); $x++) {
                            echo "$f1teams[$x] <br>";
                        }

                        ?>
                    </div>
                </div>
            </div> <!-- end card -->

            <div class="card" style="width: 65%"
            ">
            <div class="card-title"><h1 class="h1">Voorbeeld Array</h1></div>
            <img class="card-img" src="/views/img/php-array2.png" alt="php array">
            <div class="card-img-overlay">
                <div class="card-body">
                    <?php
                    echo "<br>";
                    ?>
                    <h3 class='h3'>Foreach loop</h3>
                    <code>$kites = ['Gaastra', 'F-One', 'Cabrinha', 'Gin'];</code>
                    <br>
                    <code>var_dump($kites);</code>
                    <br>
                    <code>
                        foreach ($kites as $kite) {<br>
                        &nbsp;&nbsp;echo "$kite";<br>
                        }<br>
                    </code>
                    <h3 class="h3">For loop</h3>
                    <br><code>$f1teams = array('Red Bull', 'Mclaren', 'Ferrari');</code>
                    <br>
                    <code>var_dump($f1teams);</code>
                    <br>
                    <code>
                        for($x = 0; $x < count($f1teams); $x++) {<br>
                        &nbsp;&nbsp;echo "$f1team[$x]";<br>
                        }<br>
                    </code>


                </div>
            </div>
        </div><!-- end card -->
        <div class="card" style="width: auto">
            <div class="card-title"><h4 class="h4">Opdracht Array</h4></div>
            <img src="/views/img/php-array.png" class="card-img-top" style="width: 18rem" alt="...">
            <div class="card-body">
                <ol class="list-group">
                    <li class="list-group-item">Maak een variable $ideApps. Vul deze met de verschillende IDE's waar
                        jullie mee kunnen werken.
                    </li>
                    <li class="list-group-item">Doe een var_dump() van de variabele.</li>
                    <li class="list-group-item">Maak een foreach() loop en een do ... while() loop.</li>
                    <li class="list-group-item"><a href="https://pineco.de/useful-php-array-tricks/"
                                                   target="_blank">Useful array tricks</a></li>
                </ol>
            </div>
        </div><!-- end card -->

    </div> <!-- end row -->
    </div> <!-- end container-fluid -->

    <ul class="nav justify-content-end fixed-bottom">
        <li class="nav-item">
            <a class="nav-link" href="/" tabindex="-1" aria-disabled="true"><i class="fas fa-home"></i></a>
        </li>
    </ul>
<?php require 'views/elements/tail.php'; ?>