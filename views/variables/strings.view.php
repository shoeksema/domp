<?php
    require 'views/elements/head.php';
    require "views/elements/nav.php";
?>
<div class="container-fluid">
    <div class="row">
        <?php require 'views/elements/cardLocalhost.php'; ?>
    </div> <!-- End row -->
    <div class="row">
        <div class="card w-25">
            <h4 class="card-header">Voorbeeld</h4>
            <img src="views/img/String-in-PHP.jpg.jpeg" class="card-img-top" style="width: 18rem" alt="...">
            <div class="card-body">
                <?php

                echo "Welkom ADSD";

                $adsd = 2021;

                echo "<br>Welkom ADSD $adsd";
                echo '<br>Welkom ADSD $adsd';

                ?>

            </div>
        </div> <!-- End card -->
        <div class="card w-25">
            <h4 class="card-header">Uitwerking</h4>
            <img src="views/img/String-in-PHP.jpg.jpeg" class="card-img-top" style="width: 18rem" alt="...">
            <div class="card-body">
                <code>echo "Welkom ADSD";</code><br>
                <code>echo "Welkom ADSD $adsd";</code><br>
                <code>echo 'Welkom ADSD $adsd';</code>
            </div>
        </div><!-- End card -->
        <div class="card w-auto" >
            <h4 class="card-header">Opdracht</h4>
            <img src="views/img/String-in-PHP.jpg.jpeg" class="card-img-top" style="width: 18rem" alt="...">
            <div class="card-body">
                <ol class="list-group list-group-numbered">
                    <li class="list-group-item active">Opdracht</li>
                    <li class="list-group-item">Maak twee variable voor je voornaam en achternaam.</li>
                    <li class="list-group-item">Maak een zin waar je de twee waardes gebruikt.</li>
                    <li class="list-group-item">Wat is concationation? Laat het zien in een voorbeeld.</li>
                </ol>

            </div>
        </div>
    </div>
</div>

<?php   require 'views/elements/tail.php'; ?>