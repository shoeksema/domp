<?php
    require 'views/elements/head.php';
    require "views/elements/nav.php";
?>
<div class="container-fluid px-5">
    <div class="row">
        <?php require 'views/elements/cardLocalhost.php'; ?>
    </div><!-- end row -->
    <div class="row">

        <div class="card w-25">
            <h4 class="card-header">Voorbeeld</h4>
            <img src="/views/img/PHP-Integer.png" class="card-img-top" style="width: 18rem" alt="...">
            <div class="card-body">
                <?php
                    echo "Welkom ADSD " . 2021;

                    $startADSD = 2016;
                    echo "<br>ADSD in Almer is gestart in $startADSD";
                    echo "<br>De opleiding bestaat: " . 2021 - $startADSD . " jaar!";

                ?>
            </div>
        </div> <!-- end card -->

        <div class="card w-50"">
        <h4 class="card-header">Uitwerking</h4>
        <img src="/views/img/PHP-Integer.png" class="card-img-top" style="width: 18rem" alt="...">
        <div class="card-body">
            <code>echo "Welkom ADSD " . 2021;</code>
            <br><code>echo "ADSD in Almer is gestart in $startADSD";</code>
            <br><code>echo "De opleiding bestaat: " . 2021 - $startADSD . " jaar!";</code>
        </div>
    </div><!-- end card -->
    <div class="card w-25">
        <h4 class="card-header">Opdracht</h4>
        <img src="/views/img/PHP-Integer.png" class="card-img-top" style="width: 18rem" alt="...">
        <div class="card-body">
            <ol class="list-group list-group-numbered">
                <li class="list-group-item active">Opdracht</li>
                <li class="list-group-item">Maak twee variable voor je leeftijd en duur van de opleiding.</li>
                <li class="list-group-item">Maak een zin waar je de twee waardes gebruikt.</li>
                <li class="list-group-item">Hoe oud ben je wanneer je klaar bent met de opleiding.</li>
            </ol>
        </div>
    </div><!-- end card -->

</div> <!-- end row -->

</div> <!-- end container-fluid -->

<ul class="nav justify-content-end fixed-bottom">
    <li class="nav-item">
        <a class="nav-link" href="/" tabindex="-1" aria-disabled="true"><i class="fas fa-home"></i></a>
    </li>
</ul>
<?php   require 'views/elements/tail.php'; ?>