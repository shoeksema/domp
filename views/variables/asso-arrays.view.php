<?php
require 'views/elements/head.php';
require "views/elements/nav.php";
?>
    <div class="container-fluid p-5">
        <div class="row">
            <?php require 'views/elements/cardLocalhost.php'; ?>
        </div><!-- end row -->
        <div class="row">
            <div style="text-align:center">
                <h1 class="h1">Associative Array</h1>

                <div class="ratio ratio-16x9">
                    <iframe width="560" height="315" src="https://www.youtube.com/embed/kZQTMt75-QM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
            </div>

        </div>
        <div class="row">
            <div class="card-group mb-4">
                <div class="card">
                    <div class="card-title">
                        <h1 class="h1">Voorbeeld Assosiative Array</h1>
                    </div>

                    <div class="card-body">
                        <?php
                        echo "<br>";
                        $teachers =
                            [
                                "Rudy" => [
                                    "lastname" => "Borgstede"],
                                "Stephan" => [
                                    "lastname" => "Hoeksema"]
                            ];
                        echo "<br>";
                        echo "<pre>";
                        var_dump($teachers);
                        echo "</pre>";
                        echo "<br>";
                        foreach ($teachers as $key => $value) {
                            echo $key . " " . $value['lastname'] . "<br>";
                        }
                        ?>
                    </div>

                </div> <!-- end card -->

                <div class="card">
                    <div class="card-title">
                        <h1 class="h1">uitwerking Associative Array</h1>
                    </div>
                    <div class="card-body">
                        <?php
                        echo "<br>";
                        ?>
                        <h3 class='h3'>Array</h3>
                        <code>$teachers = [ <br>
                            &nbsp;&nbsp;"Rudy" => [ <br>
                            &nbsp;&nbsp;&nbsp;&nbsp;"lastname" => "Borgstede"],<br>
                            &nbsp;&nbsp;"Stephan" => [<br>
                            &nbsp;&nbsp;&nbsp;&nbsp;"lastname" => "Hoeksema"]<br>
                            ];<br></code>
                        <br>
                        <code>var_dump($teachers);</code>
                        <br>
                        <br>
                        <code>
                            foreach ($teachers as $key => $value) {<br>
                            &nbsp;&nbsp;&nbsp;&nbsp;echo $key . " " . $value['lastname'] ; <br>
                            }<br>
                        </code>

                    </div>
                </div><!-- end card -->
                <div class="card">
                    <div class="card-title"><h4 class="h4">Opdracht Array</h4></div>
                    <img src="views/img/asso-arrays.jpeg" class="card-img-top" style="width: 18rem" alt="...">
                    <div class="card-body">
                        <ol class="list-group">
                            <li class="list-group-item">Maak een variable $sports. Vul deze met de verschillende
                                sporten.
                            </li>
                            <li class="list-group-item">In de lijst van de sporten komen de favorieten te staan (vb: f1:
                                red-bull=>
                                verstappen, perez).
                            </li>
                            <li class="list-group-item">Maak een foreach() loop: $key $value Laat de teams zien en hun
                                <leden class=""></leden>
                            </li>
                            <li class="list-group-item"><a href="https://pineco.de/useful-php-array-tricks/"
                                                           target="_blank">Useful array tricks</a></li>
                        </ol>
                    </div>
                </div><!-- end card -->
            </div><!-- end card-group -->
        </div><!-- end row cards  -->

    </div> <!-- end container-fluid -->

    <ul class="nav justify-content-end fixed-bottom">
        <li class="nav-item">
            <a class="nav-link" href="/" tabindex="-1" aria-disabled="true"><i class="fas fa-home"></i></a>
        </li>
    </ul>
<?php require 'views/elements/tail.php'; ?>