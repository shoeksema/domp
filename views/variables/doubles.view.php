<?php
require "views/elements/head.php";
require "views/elements/nav.php";
?>
<div class="container-fluid px-5">
    <div class="row">
        <?php require 'views/elements/cardLocalhost.php'; ?>
    </div><!-- end row -->
    <div class="row">

        <div class="card" style="width: 25rem;">
            <h4 class="card-header">Voorbeeld gebruik van double</h4>
            <img src="/views/img/phpvar.png" class="card-img-top" style="width: 18rem" alt="...">
            <div class="card-body">
                <?php
                    echo "<ol>
                        <li>Komma getallen: &euro;" . 5025.56 . "</li>";
                        $weight = 98.7;
                        $noStudentsWithJobs = (71 / 84 ) * 100;
                        echo "<li>Mijn gewicht: $weight kg </li>";
                        echo "<li>
                            Percentage studenten met een baan " .
                            number_format((float)$noStudentsWithJobs, 2, '.', '') . "%!!!
                        </li>
                    </ol>";

                ?>
            </div>
        </div> <!-- end card -->

        <div class="card" style="width: auto"">
            <h4 class="card-header">Uitwerking</h4>
            <img src="/views/img/phpvar.png" class="card-img-top" style="width: 18rem" alt="...">
            <div class="card-body">
                <code>echo "Komma getallen: &euro;" . 5025.56;</code><br>
                <code>$weight = 98.7;</code><br>
                <code>$noStudentsWithJobs = (71 / 84 ) * 100;</code><br>
                <code>echo "Mijn gewicht: $weight kg";</code><br>
                <code>echo "Percentage studenten met een baan "</code><br><code> . number_format((float)$noStudentsWithJobs, 2, '.', '') . "%!!!";</code>

            </div>
        </div><!-- end card -->
        <div class="card" style="width: auto">
            <h4 class="card-header">Opdracht</h4>
            <img src="/views/img/phpvar.png" class="card-img-top" style="width: 18rem" alt="...">
            <div class="card-body">
                <ul class="list-group list-group">
                    <li class="list-group-item active">Opdracht</li>
                    <li class="list-group-item">
                        <details>
                            <summary>Bereken het percentage van de totale studietijd, gekeken naar de twee jaar van de ADSD, die je nu al hebt afgerond.</summary>
                            <p>Gemiddeld is een student tussen de 40 à 50 uur met zijn studie per week bezig! Exclusief de vakanties!</p>
                        </details>
                    </li>
                </ul>
            </div>
        </div><!-- end card -->
    </div> <!-- end row -->
</div> <!-- end container-fluid -->

<ul class="nav justify-content-end fixed-bottom">
    <li class="nav-item">
        <a class="nav-link" href="/" tabindex="-1" aria-disabled="true"><i class="fas fa-home"></i></a>
    </li>
</ul>
<?php   require 'views/elements/tail.php'; ?>