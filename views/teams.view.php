<?php


require 'elements/head.php';
require 'elements/nav.php';
?>
    <div class="container-fluid p-5">
        <div class="row">
            <?php require 'views/elements/cardLocalhost.php'; ?>
        </div> <!-- End row -->

        <div class="row">
            <div class="col-2"></div>
            <div class="col-8"><h2>Welkom bij de teams pagina van 2021.</h2></div>
            <div class="col-2"></div>
        </div>
        <div class="row">
            <div class="col-2"></div>
            <div class="col-8">
                <div class="container py-5 h-100">
                    <div class="row d-flex justify-content-center align-items-center h-100">
                        <div class="col-12">

                            <div class="card text-white" style="background-color: #1f959b; border-radius: 15px;">
                                <div class="card-body p-2" style="width: available">

                                    <i class="fas fa-quote-left fa-2x mb-4"></i>

                                    <p class="lead">“The most important moments are the ones that make you realize there’s no turning back. You’ve crossed a line, and you’re stuck on the other side now.”</p>

                                    <hr>

                                    <div class="d-flex justify-content-between">
                                        <p class="mb-0">Tokyo</p>
                                        <h6 class="mb-0"><span class="badge rounded-pill" style="background-color: rgba(0,0,0, 0.2);"></span></h6>
                                    </div>

                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div class="col-2"></div>
        </div>
        <div class="row">
            <div class="col-2"></div>
            <div class="col-8">
                <h3>Download setup project: <a href="wfflix.zip">Project</a></h3>
                <h4>Download <a href="/views/mvc/Teacher.php.zip">Teacher</a> model.</h4>
            </div>
            <div class="col-2"></div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="card mb-3">
                <div class="row g-0">
                    <div class="col-md-4 offset-1">
                        <img
                                src="views/img/teams/A1.png"
                                alt="..."
                                class="img-fluid w-75"
                        />
                    </div>
                    <div class="col-md-7">
                        <div class="card-body">
                            <h5 class="card-title">Team A1</h5>
                            <p class="card-text">
                                This is a wider card with supporting text below as a natural lead-in to
                                additional content. This content is a little bit longer.
                            </p>
                            <p class="card-text">
                                <small class="text-muted">Last updated 3 mins ago</small>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="card mb-3">
                <div class="row g-0 align-items-center text-end">
                    <div class="col-md-8 ps-2">
                        <div class="card-body">
                            <h5 class="card-title">Team A2</h5>
                            <p class="card-text">
                                This is a wider card with supporting text below as a natural lead-in to
                                additional content. This content is a little bit longer.
                            </p>
                            <p class="card-text">
                                <small class="text-muted">Last updated 3 mins ago</small>
                            </p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <img
                                src="views/img/teams/A2.jpg"
                                alt="..."
                                class="img-fluid w-100"
                        />
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php  require 'elements/tail.php';