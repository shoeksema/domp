<?php
    require 'views/elements/head.php';
    require "views/elements/nav.php";
?>
<div class="container-fluid">
    <div class="row">
        <?php require 'views/elements/cardLocalhost.php'; ?>
    </div> <!-- End row -->
    <div class="row">
        <div class="card" style="width: 20rem;">
            <h4 class="card-header">Voorbeeld</h4>
            <img src="/views/img/String-in-PHP.jpg.jpeg" class="card-img-top" style="width: 18rem" alt="...">
            <div class="card-body">
                <?php
                /**
                 * Op een command prompt of een terminal en start een lokale server.
                 * Om dit te doen:
                 * 1. Ga in de directory staan waar je de root van je applicatie is.
                 * 2. Type: php -S localhost:8000
                 * 3. maak in je project directory een string.view.php.
                 * 4. Wanneer je nu in je webbrowser naar het adres localhost:8000 gaat zie je de
                 *    string.view.php die jij gedefinieerd hebt.
                 *
                 * Created by: Stephan Hoeksema 2021
                 * Welke variable types zijn er?
                 * Altijd afsluiten met een semi-colo!!!!!!!!!!!!!!!`
                 */

                /**
                 * String
                 * Direct of met een variable
                 */
                echo "Welkom ADSD";

                $adsd = 2021;
                /**
                 * Met double-quotes(") kan je de waarde van een variabele laten zien.
                 * Met single-quotes(') laat je de naam van de variabele zien!
                 */
                echo "<br>Welkom ADSD $adsd";
                echo '<br>Welkom ADSD $adsd';

                /**
                 * Opdracht
                 * 1. Maak twee variable voor je voornaam en achternaam.
                 * 2. Maak een zin waar je de twee waardes gebruikt.
                 * 3. Wat is concationation? Laat zien in een voorbeeld.
                 */

                ?>

            </div>
        </div> <!-- End card -->
        <div class="card" style="width: 25rem;">
            <h4 class="card-header">Uitwerking</h4>
            <img src="/views/img/String-in-PHP.jpg.jpeg" class="card-img-top" style="width: 18rem" alt="...">
            <div class="card-body">
                <?php
                /**
                 * Op een command prompt of een terminal en start een lokale server.
                 * Om dit te doen:
                 * 1. Ga in de directory staan waar je de root van je applicatie is.
                 * 2. Type: php -S localhost:8000
                 * 3. maak in je project directory een string.view.php.
                 * 4. Wanneer je nu in je webbrowser naar het adres localhost:8000 gaat zie je de
                 *    string.view.php die jij gedefinieerd hebt.
                 *
                 * Created by: Stephan Hoeksema 2021
                 * Welke variable types zijn er?
                 * Altijd afsluiten met een semi-colo!!!!!!!!!!!!!!!`
                 */

                /**
                 * String
                 * Direct of met een variable
                 */
                ?>
                <code>echo "Welkom ADSD";</code>
                <?php
                $adsd = 2021;
                /**
                 * Met double-quotes(") kan je de waarde van een variabele laten zien.
                 * Met single-quotes(') laat je de naam van de variabele zien!
                 */
                ?>
                <code>echo "<br>Welkom ADSD $adsd";</code>
                <code>echo '<br>Welkom ADSD $adsd';</code>
                <?php

                /**
                 * Opdracht
                 * 1. Maak twee variable voor je voornaam en achternaam.
                 * 2. Maak een zin waar je de twee waardes gebruikt.
                 * 3. Wat is concationation? Laat zien in een voorbeeld.
                 */

                ?>

            </div>
        </div><!-- End card -->
        <div class="card" style="width: auto;">
            <h4 class="card-header">Opdracht</h4>
            <img src="/views/img/String-in-PHP.jpg.jpeg" class="card-img-top" style="width: 18rem" alt="...">
            <div class="card-body">
                <ol class="list-group list-group-numbered">
                    <li class="list-group-item active">Opdracht</li>
                    <li class="list-group-item">Maak twee variable voor je voornaam en achternaam.</li>
                    <li class="list-group-item">Maak een zin waar je de twee waardes gebruikt.</li>
                    <li class="list-group-item">Wat is concationation? Laat het zien in een voorbeeld.</li>
                </ol>

            </div>
        </div>
    </div>
</div>
<ul class="nav justify-content-end fixed-bottom">
    <li class="nav-item">
        <a class="nav-link" href="/" tabindex="-1" aria-disabled="true"><i class="fas fa-home"></i></a>
    </li>
</ul>
<?php   require 'views/elements/tail.php'; ?>