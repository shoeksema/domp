<?php
    require 'views/elements/head.php';
    require "views/elements/nav.php";
?>
<div class="container-fluid">
    <div class="row">
        <?php require 'views/elements/cardLocalhost.php'; ?>
    </div> <!-- End row -->
    <div class="row">
        <div class="card" style="width: 40rem;">
            <div class="card-body">
                <iframe width="560" height="315" src="https://www.youtube.com/embed/jvPem8hB9fg" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
        </div> <!-- End card -->

    </div>
</div>
<?php   require 'views/elements/tail.php'; ?>