<?php


require 'views/elements/head.php';
require 'views/elements/nav.php';
?>
    <div class="container-fluid p-5">

        <div class="container-fluid">
            <div class="row">
                <?php require 'views/elements/cardLocalhost.php'; ?>
            </div> <!-- End row -->
            <div class="row">
                <div class="card" style="width: 40rem;">
                    <h4 class="card-header">Tutorial</h4>
                    <div class="card-body">
                        <div style="text-align:center">
                            <div class="ratio ratio-16x9">
                                <iframe width="560" height="315" src="https://www.youtube.com/embed/LanxHcFF3LU" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            </div>
                        </div>


                    </div>
                </div> <!-- End card -->

                <div class="card" style="width: auto;">
                    <h4 class="card-header">Opdracht</h4>
                    <div class="card-body">
                        <ol class="list-group ">
                            <li class="list-group-item active">Opdracht</li>
                            <li class="list-group-item">Een course heeft meerder video's, wat hebben docenten? 1 of meer studenten en hebben studenten meerder docenten? Teken je schema!!!!</li>
                        </ol>

                    </div>
                </div>
            </div>
        </div>
        <ul class="nav justify-content-end fixed-bottom">
            <li class="nav-item">
                <a class="nav-link" href="/" tabindex="-1" aria-disabled="true"><i class="fas fa-home"></i></a>
            </li>
        </ul>
    </div>

<?php  require 'views/elements/tail.php';