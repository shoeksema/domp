<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container-fluid">
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <a class="navbar-brand" href="/">PHP</a>
        <div class="collapse navbar-collapse" id="navbarTogglerDemo03">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                <li class="nav-item dropdown">
                    <a href="#" class="nav-link dropdown-toggle" data-bs-toggle="dropdown" role="button" aria-expanded="false">Variables</a>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="strings">Strings</a>
                        <a class="dropdown-item" href="integers">Integers</a>
                        <a class="dropdown-item" href="doubles">Doubles</a>
                        <a class="dropdown-item disabled"  href="#">Boolean</a>
                        <a class="dropdown-item"  href="arrays">Array's</a>
                        <a class="dropdown-item"  href="associative-arrays">Associative Array's</a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a href="#" class="nav-link dropdown-toggle" data-bs-toggle="dropdown" role="button" aria-expanded="false">MVC</a>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="sor">SOR</a>
                        <hr class="dropdown-divider">
                        <a class="dropdown-item" href="sor-nav">SOR NAV</a>
                        <a class="dropdown-item" href="sor-hf">SOR Head & Foot</a>
                        <hr class="dropdown-divider">
                        <a class="dropdown-item" href="data">Views</a>
                        <a class="dropdown-item" href="routing">Routing</a>
                        <a class="dropdown-item" href="config">Config</a>
                        <a class="dropdown-item disabled"  href="/views/mvc/controllers.php">Controllers</a>
                        <a class="dropdown-item disabled"  href="/views/mvc/models.php">Models</a>

                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a href="#" class="nav-link dropdown-toggle" data-bs-toggle="dropdown" role="button" aria-expanded="false">DB</a>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="create-database">Create DB/Tables</a>
                        <a class="dropdown-item" href="relations-1o1">Relations 1 on 1</a>
                        <a class="dropdown-item" href="relations-1om">Relations 1 on Many</a>
                        <a class="dropdown-item disabled" href="#">Relations Many on Many</a>
                        <a class="dropdown-item" href="db-connection">DB Connection</a>
                    </div>
                </li>


                <li class="nav-item">
                    <a class="nav-link" href="/views/BS5-LearnerKit/index.html" target="_blank">Bootstrap 5 learner kit</a>
                </li>
            </ul>
        </div>
    </div>
</nav>