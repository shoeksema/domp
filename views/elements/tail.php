<!-- Bootstrap Bundle with Popper -->
<footer class="mt-auto bg-dark text-white p-3">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-9 text-center">

                <div class="copyright">
                    <p class="mb-3">© Hogeschool Windesheim. All Rights Reserved.</p>
                </div>
            </div>
        </div>
    </div>
</footer>


<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM"
        crossorigin="anonymous"></script>

</body>
</html>