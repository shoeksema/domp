<?php
$pageName = trim($_SERVER['REQUEST_URI'], '/');

if($pageName === 'sor') {
    $pageName = 'Separation of Responsibilities';
    $fileName = "sor";
} elseif ($pageName === 'data') {
    $pageName = 'Views';
    $fileName = "views-data";
}
?>
<h1 class="h1"><?= strtoupper($pageName); ?></h1>
<div>
    <button class="btn " type="button" data-bs-toggle="collapse"
            data-bs-target="#collapseWidthExample" aria-expanded="false" aria-controls="collapseWidthExample">
        <img src="/views/img/localserver.webp" class="img-thumbnail" style="height: 25px" alt="">localhost <i class="fa fa-caret-square-o-down" aria-hidden="true"></i>
    </button>
</div>
<div style="min-height: 20px;">
    <div class="collapse collapse-horizontal" id="collapseWidthExample">
        <div class="card">
            <h4 class="card-header">Local webserver</h4>
            <div class="card-body">
                <ul class="list-group">
                    <li class="list-group-item active">Op een command prompt of een terminal en start een lokale
                        server.
                    </li>
                    <li class="list-group-item">
                        Ga in de directory staan waar je de root van je applicatie is.
                    </li>
                    <li class="list-group-item">
                        Bij mij: <kbd>/Users/stephanhoeksema/projects/domp</kbd>
                    </li>
                    <li class="list-group-item">Type: <kbd>php -S localhost:8000</kbd></li>
                    <li class="list-group-item">maak in je project directory een file <kbd><?= (empty($fileName) ) ? $pageName :$fileName; ?>.php</kbd>.
                    </li>
                    <li class="list-group-item">Wanneer je nu in je webbrowser naar het adres <kbd>localhost:8000/<?= (empty($fileName) ) ? $pageName :$fileName; ?>.php</kbd>
                        gaat zie je de <?= (empty($fileName) ) ? $pageName :$fileName; ?>.php die jij gedefinieerd hebt.
                    </li>
                </ul>
            </div>
        </div><!-- end card -->
    </div>
</div> <!-- End card local webserver -->