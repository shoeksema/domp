<?php


require 'elements/head.php';
require 'elements/nav.php';
?>
    <div class="container-fluid p-5">
        <div class="row">
            <?php require 'views/elements/cardLocalhost.php'; ?>
        </div> <!-- End row -->

        <div class="row">
            <div class="col-2"></div>
            <div class="col-8"><h2>Welkom bij de introductie van php.</h2></div>
            <div class="col-2"></div>
        </div>
        <div class="row">
            <div class="col-2"></div>
            <div class="col-8">
                <div class="container py-5 h-100">
                    <div class="row d-flex justify-content-center align-items-center h-100">
                        <div class="col-12">

                            <div class="card text-white" style="background-color: #1f959b; border-radius: 15px;">
                                <div class="card-body p-2" style="width: available">

                                    <i class="fas fa-quote-left fa-2x mb-4"></i>

                                    <p class="lead">“The most important moments are the ones that make you realize there’s no turning back. You’ve crossed a line, and you’re stuck on the other side now.”</p>

                                    <hr>

                                    <div class="d-flex justify-content-between">
                                        <p class="mb-0">Tokyo</p>
                                        <h6 class="mb-0"><span class="badge rounded-pill" style="background-color: rgba(0,0,0, 0.2);"></span></h6>
                                    </div>

                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div class="col-2"></div>
        </div>
        <div class="row">
            <div class="col-2"></div>
            <div class="col-8">
                <h3>Download setup project: <a href="wfflix.zip">Project</a></h3>
                <h4>Download <a href="/views/mvc/Teacher.php.zip">Teacher</a> model.</h4>
            </div>
            <div class="col-2"></div>
        </div>
        <div class="row gx-4">
            <div class="col-2"></div>
            <div class="col-8 gx-4">
                <p>
                    De cheat sheet die hieronder beschreven is geeft verklaringen voor
                    termen die in het college naar voren komen. Wanneer je een term hebt die je niet
                    begrijpt of niet helemaal begrijpt laat die dan hieronder achter. Ik zal de term
                    zien en antwoord proberen te geven.
                </p>

                <h3>Cheat Sheet</h3>

                <dl>
                    <?php foreach ($results as $result => $value) : ?>
                    <?php //die(var_dump($value->title)); ?>
                        <dt>
                            <?php if($value->title_link) : ?>
                                <a href="<?= $value->title_link; ?>" target="_blank">
                                    <?= $value->title; ?>
                                </a>
                            <?php else : ?>
                                <?= $value->title; ?>
                            <?php endif; ?>
                        </dt>
                        <dd><?= $value->description; ?></dd>
                        <form action="upd-cs" method="post">
                            <input type="hidden" name="id" value="<?= $value->id; ?>">
                            <button class="bg-white border-0 text-white" type="submit" title="Update" >update</button>
                        </form>
                        <hr>

                    <?php endforeach; ?>
                </dl>
            </div>
            <div class="col-2"></div>
        </div>
        <div class="row">
            <div class="col-2"></div>
            <div class="col-8">
                <hr class="-ruler-horizontal">
                <form method="post" action="question">
                    <!-- Question input -->
                    <div class="form-outline mb-4">
                        <input type="text" id="question" name="question" class="form-control" />
                        <label class="form-label" for="question">Question</label>
                    </div>

                    <!-- external-link input -->
<!--                    <div class="form-outline mb-4">-->
<!--                        <input type="url" name="e-link" id="e-link" class="form-control" />-->
<!--                        <label class="form-label" for="e-link">external link</label>-->
<!--                    </div>-->

                    <!-- Description input -->
<!--                    <div class="form-outline mb-4">-->
<!--                        <textarea class="form-control" name="description" id="description" rows="4"></textarea>-->
<!--                        <label class="form-label" for="description">Explanation</label>-->
<!--                    </div>-->

                    <!-- Submit button -->
                    <button type="submit" class="btn btn-primary btn-block mb-4">Ask</button>
                </form>
            </div>
            <div class="col-2"></div>
        </div>
    </div>

<?php  require 'elements/tail.php';