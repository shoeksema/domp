<?php
/**
 * Created by: Stephan Hoeksema 2021
 * wfflix2021
 */

return [
    'database' => [
        'name' => 'wfflix',
        'user' => 'root',
        'pass' => '',
        'connection' => 'mysql:host=127.0.0.1',
        'options' => [
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
        ]
    ]
];