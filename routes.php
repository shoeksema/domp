<?php
/**
 * Pages controller
 */
$router->get('', 'PagesController@home');
$router->get('adsd2021teams', 'PagesController@adsd2021teams');

/**
 * Variables routes
 */
$router->get('strings', 'PHP_Variables@var_string');
$router->get('integers', 'PHP_Variables@var_integer');
$router->get('doubles', 'PHP_Variables@var_doubles');
$router->get('arrays', 'PHP_Variables@var_array');
$router->get('associative-arrays', 'PHP_Variables@var_assoarray');
/**
 * routes MVC
 */
$router->get('sor', 'PHP_MVC@sor');
$router->get('sor-nav', 'PHP_MVC@sornav');
$router->get('sor-hf', 'PHP_MVC@sorhf');
$router->get('data', 'PHP_MVC@dataviews');
$router->get('routing', 'PHP_MVC@mvcrouting');
$router->get('config', 'PHP_MVC@mvcconfig');
/**
 * Routes DB
 */
$router->get('create-database' , 'MysqlPHP@dbcreate');
$router->get('relations-1o1' , 'MysqlPHP@db1o1');
$router->get('relations-1om' , 'MysqlPHP@db1om');
$router->get('db-connection' , 'MysqlPHP@dbconn');

/**
 * POST routes
 */
$router->post('question', 'controllers/form/question.php');
$router->post('upd-cs', 'controllers/cheat_sheet/upd-cheat_sheet.php');
$router->post('cs-upd', 'controllers/cheat_sheet/cheat_sheet-upd.php');



