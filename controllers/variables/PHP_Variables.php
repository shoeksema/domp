<?php


class PHP_Variables
{
    public function var_string()
    {
        require 'views/variables/strings.view.php';
    }

    public function var_integer()
    {
        require 'views/variables/integers.view.php';
    }

    public function var_doubles()
    {
        require 'views/variables/doubles.view.php';
    }

    public function var_array()
    {
        require 'views/variables/arrays.view.php';
    }

    public function var_assoarray()
    {
        require 'views/variables/asso-arrays.view.php';
    }
}