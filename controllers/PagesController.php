<?php


class PagesController
{
    public function home()
    {
        $dbh = new CheatSheet(App::get('database'));
        $results = $dbh->show();


        require 'views/index.view.php';
    }

    public function adsd2021teams()
    {
        require 'views/teams.view.php';
    }
}