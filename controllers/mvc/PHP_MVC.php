<?php


class PHP_MVC
{
    public function sor()
    {
        require 'views/mvc/sor.view.php';
    }

    public function sornav()
    {
        require 'views/mvc/sor-nav.view.php';
    }

    public function sorhf()
    {
        require 'views/mvc/sor-hf.view.php';
    }

    public function dataviews()
    {
        require 'views/mvc/views.view.php';
    }

    public function mvcrouting()
    {
        require 'views/mvc/routing.view.php';
    }

    public function mvcconfig()
    {
        require 'views/mvc/config.view.php';
    }
}