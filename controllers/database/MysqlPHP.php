<?php


class MysqlPHP
{
    public function dbconn()
    {
        require 'views/database/dbconnection.view.php';
    }

    public function db1o1()
    {
        require 'views/database/r-1o1.view.php';
    }

    public function db1om()
    {
        require 'views/database/r-1oM.view.php';
    }

    public function dbcreate()
    {
        require 'views/database/create.view.php';
    }
}