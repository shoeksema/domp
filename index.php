<?php
require 'vendor/autoload.php';
$conn = require 'core/bootstrap.php';

/**
 * Add Request method
 */
Routes::load('routes.php')
    ->direct(Request::uri(), Request::method());

