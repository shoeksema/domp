<?php
/**
 * Created by: Stephan Hoeksema 2021
 * wfflix2021
 */

class CheatSheet
{
    protected $conn;

    public function __construct($conn)
    {
        $this->conn = $conn;
    }

    // CREATE
    public function create($cheat_sheet )
    {
        $stmt = $this->conn->prepare("
                    INSERT INTO cheat_sheet(title, 
                                            title_link, 
                                            description
                                            ) 
                    VALUES (?, ? , ?);
                ");

        $stmt->execute([
            $cheat_sheet['title'],
            $cheat_sheet['title_link'],
            $cheat_sheet['description']
        ]);
        header('Location: /');

    }

    // READ
    public function show()
    {
        $stmt = $this->conn->query('SELECT * FROM cheat_sheet');
        return $stmt->fetchAll(PDO::FETCH_CLASS);
    }

    // UPDATE
    public function edit($cheat_sheet)
    {

        $stmt = $this->conn->prepare('SELECT * FROM cheat_sheet WHERE id = (?);');
        $stmt->execute([$cheat_sheet['id']]);
        $cheat_sheet = $stmt->fetch(PDO::FETCH_ASSOC);
        return $cheat_sheet;
    }

    // Store
    public function store($cheat_sheet)
    {
        $stmt = $this->conn->prepare("
                    UPDATE cheat_sheet 
                    SET title = (?), 
                        title_link = (?), 
                        description = (?),
                        updatedAt = now()
                    WHERE id = (?);
                ");

        $stmt->execute([
            $cheat_sheet['title'],
            $cheat_sheet['title_link'],
            $cheat_sheet['description'],
            $cheat_sheet['id']
        ]);
        header('Location: /');
    }

    // DELETE
    public function destroy($cheat_sheet)
    {
        $stmt = $this->conn->prepare("
                    DELETE FROM cheat_sheet
                    WHERE id =  (?);
                ");

        $stmt->execute([$cheat_sheet['id']]);
        header('Location: /');
    }
}