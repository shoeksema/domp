<?php
/**
 * Created by: Stephan Hoeksema 2021
 * wfflix2021
 */

class App
{
    protected static $items = [];

    public static function bind($key, $value)
    {
        static::$items[$key] = $value;
    }

    public static function get($key)
    {
        if (! array_key_exists($key, static::$items)) {
            throw new Exception("No {$key} exists!");
        }
        return static::$items[$key];
    }
}