-- MySQL dump 10.13  Distrib 8.0.21, for osx10.15 (x86_64)
--
-- Host: 127.0.0.1    Database: wfflix
-- ------------------------------------------------------
-- Server version	8.0.21

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cheat_sheet`
--

DROP TABLE IF EXISTS `cheat_sheet`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cheat_sheet` (
  `id` int NOT NULL AUTO_INCREMENT,
  `title` varchar(64) NOT NULL,
  `title_link` varchar(64) DEFAULT NULL,
  `description` mediumtext,
  `createdAt` datetime DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cheat_sheet`
--

LOCK TABLES `cheat_sheet` WRITE;
/*!40000 ALTER TABLE `cheat_sheet` DISABLE KEYS */;
INSERT INTO `cheat_sheet` (`id`, `title`, `title_link`, `description`, `createdAt`, `updatedAt`) VALUES (1,'Trim','https://www.php.net/manual/en/function.trim.php','Is een internal function(https://www.php.net/manual/en/indexes.functions.php) van php. Deze functie zorgt ervoor dat alle ‘white spaces’ voor en achter een waarde verwijderd worden. Ook kan je een character meegeven die dan ook, zoals in ons geval, verwijderd wordt. ','2021-09-17 23:43:10','2021-09-17 23:43:10'),(2,'URI','https://nl.wikipedia.org/wiki/Uniform_resource_identifier','De uniform resource locator (URL) en de uniform resource name (URN) zijn beide voorbeelden van de URI. Een URL wordt gebruikt om anderen toegang te geven tot een bron, terwijl een URN bedoeld is om bronnen uit elkaar te houden. Een zorgvuldig geformuleerde URI kan soms gebruikt worden als URL én als URN.\n\nEen URL is een formele beschrijving om een bron te kunnen benaderen, een beetje zoals een routebeschrijving. Een URN identificeert enkel de bron met een unieke naam, deze hoeft dus geen betekenis te hebben. Webadres!\n','2021-09-17 23:43:10','2021-09-17 23:43:10'),(3,'Public function','https://www.educba.com/public-function-in-php/','Een functie is een stuk code dat wordt uitgevoerd. Door de uitvoering kan aan een bepaalde voorwaarde voldaan worden. Waar het hier om gaat wanneer je het in een class gebruikt spreken we over een public method. Dit houd in dat de functie buiten de class gebruikt kan worden maar ook binnen de class.\n\n<ul><li>Een private constant, property of method kan alleen gebruikt worden vanuit de class waar deze gedefinieerd is.</li>\n<li>Een protected constant, property of method kan alleen gebruikt worden vanuit de class waar deze gedefinieerd is. Of deze kan geërfd worden.</li>\n<li>Een public constant, property of method kan overal benaderd worden.</li></ul>\n','2021-09-17 23:43:10','2021-09-17 23:43:10'),(4,'Public static function','https://www.php.net/manual/en/language.oop5.static.php','Met static methods is het mogelijk om ze direct te ‘callen’, zonder daar eerst een instantie van te maken.<br>\n<img src=\"views/img/pubsf.png\" alt=\"public static function\" class=\"w-25\"> <br>Een static method die met een :: aangeroepen wordt. Een keer een method die eerst een instantie van de class moet hebben.','2021-09-17 23:43:10','2021-09-17 23:43:10');
/*!40000 ALTER TABLE `cheat_sheet` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `courses`
--

DROP TABLE IF EXISTS `courses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `courses` (
  `id` int NOT NULL AUTO_INCREMENT,
  `title` varchar(128) NOT NULL,
  `description` varchar(256) DEFAULT NULL,
  `createdAt` datetime DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `courses`
--

LOCK TABLES `courses` WRITE;
/*!40000 ALTER TABLE `courses` DISABLE KEYS */;
INSERT INTO `courses` (`id`, `title`, `description`, `createdAt`, `updatedAt`) VALUES (1,'OOP PHP','The base of PHP in this world!','2021-09-16 10:10:01','2021-09-16 10:10:01'),(2,'Laravel','The framework that works','2021-09-16 10:10:01','2021-09-16 10:10:01');
/*!40000 ALTER TABLE `courses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `profile`
--

DROP TABLE IF EXISTS `profile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `profile` (
  `id` int NOT NULL AUTO_INCREMENT,
  `teacher_id` int DEFAULT NULL,
  `gitlab_username` varchar(64) DEFAULT NULL,
  `website` varchar(256) DEFAULT NULL,
  `createdAt` datetime DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `teacher_id` (`teacher_id`),
  CONSTRAINT `profile_ibfk_1` FOREIGN KEY (`teacher_id`) REFERENCES `teachers` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `profile`
--

LOCK TABLES `profile` WRITE;
/*!40000 ALTER TABLE `profile` DISABLE KEYS */;
INSERT INTO `profile` (`id`, `teacher_id`, `gitlab_username`, `website`, `createdAt`, `updatedAt`) VALUES (1,4,'shoeksema','stephanhoeksema.nl','2021-09-15 12:20:56','2021-09-15 12:20:56');
/*!40000 ALTER TABLE `profile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teachers`
--

DROP TABLE IF EXISTS `teachers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `teachers` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `email` varchar(150) DEFAULT NULL,
  `school` varchar(50) DEFAULT NULL,
  `createdAt` datetime DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teachers`
--

LOCK TABLES `teachers` WRITE;
/*!40000 ALTER TABLE `teachers` DISABLE KEYS */;
INSERT INTO `teachers` (`id`, `name`, `email`, `school`, `createdAt`, `updatedAt`) VALUES (1,'Rudy','r.borgstede@windesheim.nl','Hogeschool Windesheim','2021-09-15 09:39:57','2021-09-15 09:39:57'),(2,'Martijn','m.suijkerbuijk@windesheim.nl','Hogeschool Windesheim','2021-09-15 09:39:57','2021-09-15 09:39:57'),(4,'Stephan','s.hoeksema@windesheim.nl','Hogeschool Windesheim','2021-09-15 10:32:06','2021-09-15 10:32:06');
/*!40000 ALTER TABLE `teachers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teachers_courses`
--

DROP TABLE IF EXISTS `teachers_courses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `teachers_courses` (
  `id` int NOT NULL AUTO_INCREMENT,
  `course_id` int DEFAULT NULL,
  `teacher_id` int DEFAULT NULL,
  `createdAt` datetime DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `course_id` (`course_id`),
  KEY `teacher_id` (`teacher_id`),
  CONSTRAINT `teachers_courses_ibfk_1` FOREIGN KEY (`course_id`) REFERENCES `courses` (`id`),
  CONSTRAINT `teachers_courses_ibfk_2` FOREIGN KEY (`teacher_id`) REFERENCES `teachers` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teachers_courses`
--

LOCK TABLES `teachers_courses` WRITE;
/*!40000 ALTER TABLE `teachers_courses` DISABLE KEYS */;
INSERT INTO `teachers_courses` (`id`, `course_id`, `teacher_id`, `createdAt`, `updatedAt`) VALUES (1,1,1,'2021-09-16 11:41:30','2021-09-16 11:41:30'),(2,1,4,'2021-09-16 11:41:30','2021-09-16 11:41:30'),(3,2,4,'2021-09-16 11:41:30','2021-09-16 11:41:30');
/*!40000 ALTER TABLE `teachers_courses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `videos`
--

DROP TABLE IF EXISTS `videos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `videos` (
  `id` int NOT NULL AUTO_INCREMENT,
  `course_id` int DEFAULT NULL,
  `title` varchar(128) NOT NULL,
  `youtube_link` varchar(256) DEFAULT NULL,
  `createdAt` datetime DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `course_id` (`course_id`),
  CONSTRAINT `videos_ibfk_1` FOREIGN KEY (`course_id`) REFERENCES `courses` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `videos`
--

LOCK TABLES `videos` WRITE;
/*!40000 ALTER TABLE `videos` DISABLE KEYS */;
INSERT INTO `videos` (`id`, `course_id`, `title`, `youtube_link`, `createdAt`, `updatedAt`) VALUES (1,1,'Localhost opzetten','https://youtu.be/14rDhMejPIw','2021-09-16 10:27:53','2021-09-16 10:27:53'),(2,1,'Define strings','https://youtu.be/14rDhMejPIw','2021-09-16 10:27:53','2021-09-16 10:27:53'),(3,1,'Define boolean','https://youtu.be/14rDhMejPIw','2021-09-16 10:27:53','2021-09-16 10:27:53'),(4,2,'Localhost opzetten met Laravel','https://youtu.be/14rDhMejPIw','2021-09-16 10:29:34','2021-09-16 10:29:34'),(5,2,'Generate controllers','https://youtu.be/14rDhMejPIw','2021-09-16 10:29:34','2021-09-16 10:29:34'),(6,2,'Generate models','https://youtu.be/14rDhMejPIw','2021-09-16 10:29:34','2021-09-16 10:29:34');
/*!40000 ALTER TABLE `videos` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-09-18 16:33:24
