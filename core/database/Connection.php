<?php
/**
 * Created by: Stephan Hoeksema 2021
 * wfflix2021
 */

class Connection
{
    public static function make($config)
    {
        try {
            return new PDO(
                $config['connection'] . ';dbname=' . $config['name'],
                $config['user'],
                $config['pass'],
                $config['options']

            );

        } catch (Exception $e) {
            die(var_dump($e->getMessage()));
        }
    }
}

