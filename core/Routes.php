<?php
/**
 * Created by: Stephan Hoeksema 2021
 * wfflix2021
 */

class Routes
{
    // make it public and GET array and POST array
    protected $routes = [
        'GET' => [],
        'POST' => []
    ];

    public static function load($file)
    {
        //eigen object
        $router = new self;
        //verkrijg routes.php
        require $file;

        return $router;
    }

    public function get($uri, $controller)
    {
        $this->routes['GET'][$uri] = $controller;
    }

    public function post($uri, $controller)
    {
        $this->routes['POST'][$uri] = $controller;
    }

    /**
     * Add requestType to parameters
     */
    public function direct($uri, $requestType)
    {
        //PageController@home
        return $this->controllerAction(
            ...explode('@', $this->routes[$requestType][$uri])
        );
    }

    protected function controllerAction($controller, $action)
    {

        $controller = new $controller;

        if(! method_exists($controller, $action)){
            throw new Exception(
                "{$controller} doesn't have the {$action} action!"
            );
        }
        return $controller->$action();
    }

}